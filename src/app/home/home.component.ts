import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {PetservService} from '../petserv.service';
import { Pet } from '../pet';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pets: Pet[] = [];
  constructor(private petService: PetservService) { }

  ngOnInit() {
    this.getAllPets();
  }

  getAllPets(){
    this.petService.getPets().subscribe(temp => {this.pets = temp; console.log(this.pets)});
  }

}
