export class Pet {
    constructor(
        public id: number,
        public image: string,
        public name: string,
        public description: string,
        public pettype: string,
        public status: boolean) {}
}
