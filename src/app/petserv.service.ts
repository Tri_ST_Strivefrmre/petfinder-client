import { Injectable } from '@angular/core';
import {HttpClient} from'@angular/common/http';
import { Observable } from 'rxjs';
import { Pet } from './pet'
import { map, catchError, retry } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class PetservService {
  handleError: any;

  constructor(private http: HttpClient) { }


  getPets(): Observable<Pet[]>{
    return this.http.get('http://192.168.1.104:5000/pet').pipe(
      map((data: any) => data.pets.map((item: any) => new Pet(
        item.id,
        item.image,
        item.name,
        item.description,
        item.pettype,
        item.status
      )))
    );
  }

  addPet(pet: Pet):Observable<Pet>{
  return this.http.post<Pet>('http://192.168.1.104:5000/pet', pet)
  .pipe(
    catchError(this.handleError('addPet', pet))
  );
  }
}
