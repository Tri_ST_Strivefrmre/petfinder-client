import { TestBed } from '@angular/core/testing';

import { PetservService } from './petserv.service';

describe('PetservService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PetservService = TestBed.get(PetservService);
    expect(service).toBeTruthy();
  });
});
